﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletInfo : MonoBehaviour {

    public float damage;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       

    }



    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject != null && gameObject != null)
        {

            if (collision.gameObject.tag == "Enemy" && gameObject.tag == "Player")
            {
                collision.gameObject.GetComponent<EnemyInfo>().HP -= damage;
                Destroy(gameObject);
            }
            else if (collision.gameObject.tag == "Player" && gameObject.tag == "Enemy")
            {
                collision.gameObject.GetComponent<ShipMovement>().HP -= damage;
            }
        }
    }
}
