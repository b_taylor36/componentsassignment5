﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    public GameObject bulletEmitter;
    public GameObject bullet;
    public float bulletForce;
    private float time;
    // Use this for initialization
    void Start () {
        time = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;

       int randNum = Random.Range(0, 10);


        //Vector3 targetDir = playerToShoot.GetComponent<Rigidbody>().position - transform.position;
        //float step = rotateSpeed * Time.deltaTime;
        //Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
        //transform.rotation = Quaternion.LookRotation(newDir);

        //bulletEmitter.transform.LookAt(playerToShoot.transform.position);
        

        if (time > 3.0f)
        {
            if (randNum % 1 == 0)
            {
                Shoot();
            }
            time = 0.0f;
        }

    }

    void Shoot()
    {
        
            GameObject tempBullet;
            tempBullet = Instantiate(bullet, bulletEmitter.transform.position, bulletEmitter.transform.rotation) as GameObject;

            Rigidbody tempRB;
            tempRB = tempBullet.GetComponent<Rigidbody>();

            tempRB.AddForce(tempRB.transform.up * bulletForce);

            DestroyObject(tempBullet, 10.0f);
           
        
    }
}
