﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShooting : MonoBehaviour {

    public GameObject bulletEmitter;
    public GameObject bulletEmitterBack;
    public GameObject bullet;
    public AudioSource audioSource;
    public float bulletForce;
    
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Shoot();

    }

    void Shoot()
    {

        if (bulletEmitter != null)
        {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                audioSource.Play();
                GameObject tempBullet;
                tempBullet = Instantiate(bullet, bulletEmitter.transform.position, bulletEmitter.transform.rotation) as GameObject;

                GameObject tempBullet2;
                tempBullet2 = Instantiate(bullet, bulletEmitterBack.transform.position, bulletEmitterBack.transform.rotation) as GameObject;

                Rigidbody tempRB;
                tempRB = tempBullet.GetComponent<Rigidbody>();

                Rigidbody tempRB2;
                tempRB2 = tempBullet2.GetComponent<Rigidbody>();

                tempRB.AddForce(tempRB.transform.up * bulletForce);
                tempRB2.AddForce(tempRB2.transform.up * -bulletForce);

                DestroyObject(tempBullet, 10.0f);
                DestroyObject(tempBullet2, 10.0f);
            }
        }
    }
}
