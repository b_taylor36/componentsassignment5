﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    public float speed;
    private Rigidbody rb;
    public float HP;
    public ParticleSystem ps;
    private double time;
    // public Vector2 screenHalfSize;
    // public GameObject boundingObject;

    void Start () {
    //    screenHalfSize = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
        rb = GetComponent<Rigidbody>();
        time = 0.0;
	}
	
	
	void Update () {

       

        if (gameObject != null)
        {
            if (HP <= 0.0f)
            {
                Instantiate(ps, transform.position, transform.rotation);
                gameObject.SetActive(false);
                time += Time.deltaTime;

                if(time > 5.0)
                {
                    time = 0.0;
                    HP = 1.0f;
                    gameObject.transform.position = new Vector3( 0.0f, 0.0f,0.0f);
                    gameObject.SetActive(true);
                }

            }

            //Movement
            float hAxis = Input.GetAxis("Horizontal");
            float vAxis = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(hAxis, vAxis, 0) * speed * Time.deltaTime;

            rb.MovePosition(transform.position + movement);







            if (Camera.main.transform.position.x < -57.0f)
            {
                transform.position = new Vector2(56.0f, transform.position.y);
            }
            else if (Camera.main.transform.position.x > 57.0f)
            {
                transform.position = new Vector2(-56.0f, transform.position.y);
            }

            if (Camera.main.transform.position.y < -59.0f)
            {
                transform.position = new Vector2(transform.position.x, 58.0f);
            }
            else if (Camera.main.transform.position.y > 59.0f)
            {
                transform.position = new Vector2(transform.position.x, -58.0f);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
                rb.AddTorque(Vector3.forward * (speed / 15.0f) * Time.deltaTime);
            else if (Input.GetKey(KeyCode.RightArrow))
                rb.AddTorque(Vector3.back * (speed / 15.0f) * Time.deltaTime);
        }
    }


}
