﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInfo : MonoBehaviour {

    public float HP;
    public ParticleSystem ps;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(HP <= 0.0f)
        {
            
            Instantiate(ps, transform.position, transform.rotation);
            //ps.Play();
            Destroy(gameObject);
        }

	}
}
