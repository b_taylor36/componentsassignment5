#Assigment Name
Game Component Integration Assignment 5

#Creator
Benjamin Taylor

#Due Date
January 16th, 2018

#How to Use
Open the project in the Unity editor, set the aspect ratio to Standalone 1024x768,
then set the game to Maximize when started.
The reason for doing this is because there's a weird bug where if the project is built
and ran, the minimap doens't display the motherships properly. The game is still playable
however its a minor annoyance.

#Controls
- Move in directions with WASD
- Change orientation with LEFT and RIGHT ARROW KEYS
- fire a laser with SPACEBAR


#Objective
- The objective of the game is to destroy all of the motherships before they can destroy
you! Each of their outer pods takes 4 shots to destroy, but if you can get a good shot
into the middle central control pod, it will destroy the whole ship!
- Getting hit with a mothership's yellow laser spells disaster and Game Over for the player
